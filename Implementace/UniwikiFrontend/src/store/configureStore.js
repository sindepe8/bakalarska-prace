import { createStore, combineReducers } from 'redux';
import postReducer from '../reducers/postReducer';
import modalReducer from '../reducers/modalReducer';
import userReducer from '../reducers/userReducer';
import addPostReducer from '../reducers/addPostReducer';

// Store creation

const configureStore = () => {
  const store = createStore(
    combineReducers({
      post: postReducer,
      addPost: addPostReducer,
      modal: modalReducer,
      user: userReducer
    })
  );

  return store;
};

export default configureStore;

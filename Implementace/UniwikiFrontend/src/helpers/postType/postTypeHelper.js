import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  ExperiencePostType,
  FreePostType,
  SemesterWorkPostType,
  StudyMaterialPostType,
  TestPostType,
  ExamPostType
} from '../../constants/postTypes';

const getNameForPostType = postType => {
  switch (postType) {
    case ExperiencePostType:
      return 'Zkušenost';
    case FreePostType:
      return 'Volný příspěvek';
    case SemesterWorkPostType:
      return 'Práce v semestru';
    case StudyMaterialPostType:
      return 'Studijní materiál';
    case TestPostType:
      return 'Test';
    case ExamPostType:
      return 'Zkouška';
    default:
      throw new Error('Enum not exhausted.');
  }
};

const getIconNameForPostType = postType => {
  switch (postType) {
    case ExperiencePostType:
      return ['fas', 'grin-wink'];
    case FreePostType:
      return ['fas', 'sticky-note'];
    case SemesterWorkPostType:
      return ['fas', 'project-diagram'];
    case StudyMaterialPostType:
      return ['fas', 'file-alt'];
    case TestPostType:
      return ['fas', 'tasks'];
    case ExamPostType:
      return ['fas', 'pencil-ruler'];
    default:
      throw new Error('Enum not exhausted.');
  }
};

const PostTypeIcon = props => (
  <FontAwesomeIcon
    icon={getIconNameForPostType(props.postType)}
    className={props.className}
  />
);

export { PostTypeIcon, getNameForPostType, getIconNameForPostType };

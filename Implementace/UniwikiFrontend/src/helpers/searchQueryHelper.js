import queryString from 'query-string';
import DataService from '../services/dataService';
import { SearchQueryModel } from '../models/searchQueryModel';

export const deserializeQuery = (matchParams, urlParamsString) => {
  // Get match params
  let universityName = matchParams.universityName;
  let facultyName = matchParams.facultyName;
  let courseCode = matchParams.courseCode;

  // Get objects for names
  let university = undefined;
  let faculty = undefined;
  let course = undefined;

  university = DataService.getUniversities().find(
    u => u.urlName === universityName
  );

  if (university) {
    faculty = university.faculties.find(f => f.urlName === facultyName);
  }

  if (faculty) {
    course = faculty.courses.find(c => c.urlCode === courseCode);
  }

  // Parse url params
  let urlParams = queryString.parse(urlParamsString);
  let queryText = decodeURIComponent(urlParams.q || '');
  if (!!!queryText) queryText = '';
  let postTypeName = urlParams.postType;
  let SortByName = urlParams.sortBy;

  return new SearchQueryModel(
    university,
    faculty,
    course,
    queryText,
    undefined,
    undefined
  );
};

export const serializeQuery = query => {
  var result = '';

  if (query.university) {
    result += '/' + query.university.urlName;

    if (query.faculty) {
      result += '/' + query.faculty.urlName;

      if (query.course) {
        result += '/' + query.course.urlCode;
      }
    }
  }

  if (query.queryText) {
    result += '?q=' + encodeURIComponent(query.queryText);
  }

  return result;
};

export const getLinkUrl = query => '/search' + serializeQuery(query);

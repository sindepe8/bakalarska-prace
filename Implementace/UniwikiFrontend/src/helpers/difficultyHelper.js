import React from 'react';

function getDotIcon(dotNum, difficulty) {
  if (dotNum <= difficulty) return ['far', 'dot-circle'];
  return ['far', 'circle'];
}

export { getDotIcon };

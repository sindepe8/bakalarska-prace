class ProfileLocationModel {
  constructor(university, faculty, studium) {
    this._university = university;
    this._faculty = faculty;
    this._studium = studium;
  }
}

export default ProfileLocationModel;

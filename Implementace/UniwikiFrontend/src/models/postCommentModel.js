class PostCommentModel {
  constructor(id, profile, text, likesCount, creationTime) {
    this._id = id;
    this._profile = profile;
    this._text = text;
    this._likesCount = likesCount;
    this._creationTime = creationTime;
  }

  get id() {
    return this._id;
  }
  get profile() {
    return this._profile;
  }
  get text() {
    return this._text;
  }
  get likesCount() {
    return this._likesCount;
  }
  get creationTime() {
    return this._creationTime;
  }
}

export default PostCommentModel;

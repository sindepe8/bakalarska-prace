class UniversityModel {
  constructor(id, name, fullName, faculties, img, imgAlt) {
    this._id = id;
    this._name = name;
    this._fullName = fullName;
    faculties.map(faculty => faculty.setUniversity(this));
    this._faculties = faculties;
    this._img = img;
    this._imgAlt = imgAlt;
  }

  get id() {
    return this._id;
  }

  get name() {
    return this._name;
  }

  get urlName() {
    return this._name
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '');
  }

  get fullName() {
    return this._fullName;
  }

  get faculties() {
    return this._faculties;
  }

  get img() {
    return this._img;
  }

  get imgAlt() {
    return this._imgAlt;
  }

  // setFaculties(faculties) {
  //   this.faculties = faculties;
  // }
}

export default UniversityModel;

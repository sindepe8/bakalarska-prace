class ProfileModel {
  constructor(
    id,
    profileImg,
    firstName,
    familyName,
    nickName,
    registeredMail,
    schoolMails,
    faculty
  ) {
    this._id = id;
    this._profileImg = profileImg;
    this._firstName = firstName;
    this._familyName = familyName;
    this._nickName = nickName;
    this._registeredMail = registeredMail;
    this._schoolMails = schoolMails;
    this._faculty = faculty;
  }

  get id() {
    return this._id;
  }
  get profileImg() {
    return this._profileImg;
  }
  get firstName() {
    return this._firstName;
  }
  get familyName() {
    return this._familyName;
  }
  get nickName() {
    return this._nickName;
  }
  get registeredMail() {
    return this._registeredMail;
  }
  get schoolMails() {
    return this._schoolMails;
  }
  get faculty() {
    return this._faculty;
  }

  get fullName() {
    return this._firstName + ' ' + this._familyName;
  }
}

export default ProfileModel;

class SemesterWorkPostContentModel {
  constructor(files) {
    this._files = files;
  }

  get files() {
    return this._files;
  }
}

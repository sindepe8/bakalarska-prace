class FacultyModel {
  constructor(id, name, fullName, courses = []) {
    this._id = id;
    this._name = name;
    this._fullName = fullName;
    this._courses = courses;
  }

  get id() {
    return this._id;
  }
  get name() {
    return this._name;
  }
  get urlName() {
    return this._name
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '');
  }
  get fullName() {
    return this._fullName;
  }
  get courses() {
    return this._courses;
  }
  get university() {
    return this._university;
  }

  setUniversity(university) {
    this._university = university;
  }
  setCourses(courses) {
    this._courses = courses;
  }
}

export default FacultyModel;

class PostModel {
  constructor(
    id,
    postType,
    course,
    profile,
    creationTime,
    text,
    likesCount,
    comments,
    content
  ) {
    this._id = id;
    this._postType = postType;
    this._course = course;
    this._profile = profile;
    this._creationTime = creationTime;
    this._text = text;
    this._likesCount = likesCount;
    this._comments = comments;
    this._content = content;
  }

  get id() {
    return this._id;
  }

  get postType() {
    return this._postType;
  }

  get course() {
    return this._course;
  }

  get profile() {
    return this._profile;
  }

  get creationTime() {
    return this._creationTime;
  }

  get text() {
    return this._text;
  }

  get likesCount() {
    return this._likesCount;
  }

  get comments() {
    return this._comments;
  }

  get content() {
    return this._content;
  }
}

export default PostModel;

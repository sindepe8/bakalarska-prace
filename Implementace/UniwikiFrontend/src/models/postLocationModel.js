class PostLocationModel {
  constructor(university, faculty, course) {
    this._university = university;
    this._faculty = faculty;
    this._course = course;
  }

  get university() {
    return this._university;
  }

  get faculty() {
    return this._faculty;
  }

  get course() {
    return this._course;
  }
}

export default PostLocationModel;

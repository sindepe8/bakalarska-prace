class ExperiencePostContentModel {
  constructor(postId, semesterDifficulty, examDifficulty) {
    this._postId = postId;
    this._semesterDifficulty = semesterDifficulty;
    this._examDifficulty = examDifficulty;
  }

  get postId() {
    return this._postId;
  }

  get semesterDifficulty() {
    return this._semesterDifficulty;
  }

  get examDifficulty() {
    return this._examDifficulty;
  }
}

export default ExperiencePostContentModel;

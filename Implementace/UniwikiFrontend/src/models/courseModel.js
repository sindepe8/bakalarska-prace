class CourseModel {
  constructor(id, code, name, faculty) {
    this._id = id;
    this._code = code;
    this._name = name;
    this._faculty = faculty;
  }

  get id() {
    return this._id;
  }

  get code() {
    return this._code;
  }

  get urlCode() {
    return this._code
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '');
  }

  get name() {
    return this._name;
  }

  get faculty() {
    return this._faculty;
  }
}

export default CourseModel;

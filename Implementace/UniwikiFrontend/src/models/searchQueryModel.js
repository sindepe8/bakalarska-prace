export class SearchQueryModel {
  constructor(university, faculty, course, queryText = '', postType, sortBy) {
    this.university = university;
    this.faculty = faculty;
    this.course = course;
    this.queryText = queryText;
    this.postType = postType;
    this.sortBy = sortBy;
  }

  // get university() {
  //   return _university;
  // }
  // get faculty() {
  //   return _faculty;
  // }
  // get course() {
  //   return _course;
  // }
  // get queryText() {
  //   return _queryText;
  // }
  // get postType() {
  //   return _postType;
  // }
  // get sortBy() {
  //   return _sortBy;
  // }

  // setQueryText = () => {

  // }
}

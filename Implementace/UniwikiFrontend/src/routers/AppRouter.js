import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import NotFoundPage from '../components/NotFoundPage';
import Header from '../components/Header';
import MainPage from '../components/main/MainPage';
import BasicLayout from '../components/base/BasicLayout';
import Footer from '../components/Footer';
import WallPage from '../components/wall/WallPage';
import AppModal from '../components/modals/AppModal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

const AppRouter = props => (
  <BrowserRouter>
    <AppModal />
    <BasicLayout>
      <Header />
      <Switch>
        <Route path="/" component={MainPage} exact={true} />
        <Route
          path="/search/:universityName?/:facultyName?/:courseCode?"
          component={WallPage}
        />
        <Route component={NotFoundPage} />
      </Switch>
    </BasicLayout>
    <Footer />
  </BrowserRouter>
);

export default AppRouter;

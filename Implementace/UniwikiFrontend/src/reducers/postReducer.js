import { SearchQueryModel } from '../models/searchQueryModel';
import DataService from '../services/dataService';
import {
  setSearchQueryAction,
  setSearchAutocompleteTextAction,
  addPostAction
} from '../constants/postActionNames';
import PostModel from '../models/postModel';

var searchSeed = [
  'Lineární algebra',
  'Lineární optimalizace',
  'Nonlineární metody',
  'Nelineární kódy',
  'Pokročilá linearizace',
  'Programování a algoritmizace'
];

const postReducerDefaultState = {
  searchResults: {
    query: new SearchQueryModel(),
    posts: DataService.getAllPosts()
  },
  autocomplete: {
    text: '',
    results: []
  }
};

const getAutocompleteResults = text => {
  if (text === '' || text === undefined) return [];

  return searchSeed.filter(term =>
    term.toLowerCase().includes(text.toLowerCase())
  );
};

const getPosts = query => {
  return DataService.getPosts(query);
};

const updateState = (state, newQuery, isRefresh) => {
  if (newQuery.university === undefined) {
    newQuery.faculty = undefined;
  }

  if (newQuery.faculty === undefined) {
    newQuery.course = undefined;
  }

  // Check if something has changed
  if (
    !isRefresh &&
    !Object.keys(state.searchResults.query).find(
      (k, i) => newQuery[k] !== state.searchResults.query[k]
    )
  ) {
    return state;
  }

  let posts = getPosts(newQuery);
  let autocomplete = {
    text: newQuery.queryText,
    results: getAutocompleteResults(newQuery.queryText)
  };

  return {
    ...state,
    searchResults: { query: newQuery, posts },
    autocomplete
  };
};

const postReducer = (state = postReducerDefaultState, action) => {
  switch (action.type) {
    case setSearchQueryAction: {
      let query = action.query;
      let isRefresh = action.isRefresh;
      return updateState(state, query, isRefresh);
    }

    case setSearchAutocompleteTextAction: {
      return {
        ...state,
        autocomplete: {
          text: action.text,
          results: getAutocompleteResults(action.text)
        }
      };
    }

    case addPostAction:
      let post = action.post;
      let query = state.searchResults.query;
      DataService.addPost(post);
      return updateState(state, query, true);

    default:
      return state;
  }
};

export default postReducer;

import { loginUserAction } from '../constants/userActionNames';

const userReducerDefaultState = {
  loggedUser: null
};

const userReducer = (state = userReducerDefaultState, action) => {
  switch (action.type) {
    case loginUserAction:
      return {
        ...state,
        loggedUser: action.loggedUser
      };
    default:
      return state;
  }
};

export default userReducer;

import {
  showModalAction,
  hideModalAction
} from '../constants/modalActionNames';
import { ModalTypeLogin } from '../constants/modalTypes';

const modalReducerDefaultState = {
  visibleModal: null
};

const modalReducer = (state = modalReducerDefaultState, action) => {
  switch (action.type) {
    case showModalAction:
      return {
        ...state,
        visibleModal: action.modal
      };
    case hideModalAction: {
      return {
        ...state,
        visibleModal: null
      };
    }
    default:
      return state;
  }
};

export default modalReducer;

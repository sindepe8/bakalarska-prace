import {
  setPostTextAction,
  setPostContentAction,
  setPostTypeAction,
  clearAddPostAction,
  setPostContentFilesAction,
  setAssignmentQuestionAction,
  createAssignmentSubquestionAction,
  setAssignmentSubquestionAction
} from '../constants/addPostActionNames';
import {
  setAssignmentSubquestion,
  createAssignmentSubquestion
} from '../actions/addPost';

const addPostReducerDefaultState = {
  text: '',
  content: {},
  postType: undefined
};

const addPostReducer = (state = addPostReducerDefaultState, action) => {
  switch (action.type) {
    case setPostTextAction:
      return {
        ...state,
        text: action.text
      };
    case setPostContentAction:
      console.log('OLD CONTENT');
      console.log(state.content);
      console.log('NEW CONTENT');
      console.log({
        ...state.content,
        ...action.content
      });
      return {
        ...state,
        content: {
          ...state.content,
          ...action.content
        }
      };
    case setPostTypeAction:
      return {
        ...addPostReducerDefaultState,
        postType: action.postType
      };
    case setPostContentFilesAction:
      const files = state.content.files
        ? [...state.content.files, ...action.files]
        : [...action.files];

      return {
        ...state,
        content: {
          ...state.content,
          files
        }
      };

    case setAssignmentQuestionAction:
      console.log('DOING');
      const questions = state.content.questions ? state.content.questions : [];

      questions[action.question] = {
        ...questions[action.question],
        text: action.text
      };
      return {
        ...state,
        content: {
          ...state.content,
          questions
        }
      };

    case clearAddPostAction: {
      return addPostReducerDefaultState;
    }

    case createAssignmentSubquestionAction: {
      console.log('CREATING');
      const question = action.question;
      let newSubquestions = state.content.questions[question].subquestions;
      if (!newSubquestions) {
        newSubquestions = [];
      }
      newSubquestions.push({ text: '' });
      state.content.questions[question].subquestions = newSubquestions;
      return {
        ...state,
        content: {
          ...state.content
        }
      };
    }

    case setAssignmentSubquestionAction: {
      console.log('q:' + action.question + ' s:' + action.subquestion);
      state.content.questions[action.question].subquestions[
        action.subquestion
      ].text = action.text;

      return {
        ...state,
        content: {
          ...state.content
        }
      };
    }
    default:
      return state;
  }
};

export default addPostReducer;

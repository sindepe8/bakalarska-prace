import {
  setSearchQueryAction,
  setSearchAutocompleteTextAction,
  addPostAction
} from '../constants/postActionNames';

export const setSearchQuery = (query, isRefresh = false) => ({
  type: setSearchQueryAction,
  query,
  isRefresh
});

export const setSearchAutocompleteText = text => ({
  type: setSearchAutocompleteTextAction,
  text
});

export const addPost = post => ({
  type: addPostAction,
  post
});

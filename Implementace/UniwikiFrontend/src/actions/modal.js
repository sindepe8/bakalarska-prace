import { hideModalAction } from '../constants/modalActionNames';

// REQUEST_LOGIN
export const hideModal = () => ({
  type: hideModalAction
});

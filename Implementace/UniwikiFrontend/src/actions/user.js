import { showModalAction } from '../constants/modalActionNames';
import { ModalTypeLogin } from '../constants/modalTypes';
import { loginUserAction } from '../constants/userActionNames';

// REQUEST_LOGIN
export const requestLogin = () => ({
  type: showModalAction,
  modal: ModalTypeLogin
});

// REQUEST_LOGIN
export const loginUser = profile => ({
  type: loginUserAction,
  loggedUser: profile
});

import {
  setPostTextAction,
  setPostContentAction,
  setPostTypeAction,
  clearAddPostAction,
  setPostContentFilesAction,
  setAssignmentQuestionAction,
  setAssignmentSubquestionAction,
  createAssignmentSubquestionAction
} from '../constants/addPostActionNames';

export const setPostText = text => ({
  type: setPostTextAction,
  text
});

export const setPostContent = content => ({
  type: setPostContentAction,
  content
});

export const setPostType = postType => ({
  type: setPostTypeAction,
  postType
});

export const clearAddPost = () => ({
  type: clearAddPostAction
});

export const setPostContentFiles = files => ({
  type: setPostContentFilesAction,
  files
});

export const setAssignmentQuestion = (text, question) => ({
  type: setAssignmentQuestionAction,
  text,
  question
});

export const setAssignmentSubquestion = (text, question, subquestion) => ({
  type: setAssignmentSubquestionAction,
  text,
  question,
  subquestion
});

export const createAssignmentSubquestion = question => ({
  type: createAssignmentSubquestionAction,
  question
});

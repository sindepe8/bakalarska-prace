import React from 'react';
import PostFiles from './addPost/addPostContents/PostFiles';

const FreePostPostContent = props => (
  <div className="px-3 mt-3 d-flex align-items-top scroll-menu">
    <PostFiles files={props.content.files} />
  </div>
);

export default FreePostPostContent;

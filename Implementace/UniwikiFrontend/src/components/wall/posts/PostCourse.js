import React from 'react';
import CourseModel from '../../../models/courseModel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PostCourse = props => (
  <div className="text-sm">
    <FontAwesomeIcon icon={['fas', 'folder']} className="mr-2" />
    {props.course.name}
  </div>
);

export default PostCourse;

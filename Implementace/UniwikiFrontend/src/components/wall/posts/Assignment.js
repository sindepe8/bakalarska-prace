import React from 'react';

const Assignment = props => (
  <div className="text-sm">
    {props.questions &&
      props.questions.map((q, i) => (
        <div key={i} className="mb-2">
          <div>
            {i + 1}.{q.text}
          </div>
          <div className="ml-2 text-sx">
            {/* Subquestions */}
            {q.subquestions &&
              q.subquestions.map((s, si) => (
                <div key={si}>
                  {i + 1}.{si + 1}.{' ' + s.text}
                </div>
              ))}
          </div>
        </div>
      ))}
  </div>
);

export default Assignment;

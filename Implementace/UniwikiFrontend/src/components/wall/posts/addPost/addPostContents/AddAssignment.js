import React from 'react';
import {
  setAssignmentQuestion,
  createAssignmentSubquestion,
  setAssignmentSubquestion
} from '../../../../../actions/addPost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';

class AddAssignment extends React.Component {
  handleQuestionText = (text, question) => {
    this.props.dispatch(setAssignmentQuestion(text, question));
  };

  handleAddSubquestion = i => {
    this.props.dispatch(createAssignmentSubquestion(i));
  };

  handleSubquestionText = (text, question, subquestion) => {
    this.props.dispatch(setAssignmentSubquestion(text, question, subquestion));
  };

  render() {
    const questions = this.props.addPost.content.questions
      ? [...this.props.addPost.content.questions]
      : [];
    return (
      <div>
        {/* Questions */}
        {[...questions, { text: '' }].map((q, i) => (
          <div className="mb-2" key={i}>
            <div className="input-group" key={i}>
              <div className="input-group-prepend">
                <span className="input-group-text">{i + 1}.</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder={'Text ' + (i + 1) + '. otázky'}
                onChange={e => this.handleQuestionText(e.target.value, i)}
              />
              <input type="text" className="form-control" placeholder="Body" />
            </div>

            {/* Subquestions */}
            <div className="ml-2">
              {q.subquestions &&
                q.subquestions.map((s, si) => (
                  <div className="mt-2" key={si}>
                    <div className="input-group" key={i}>
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {i + 1}.{si + 1}
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={
                          'Text ' + (i + 1) + '.' + (si + 1) + '. otázky'
                        }
                        onChange={e =>
                          this.handleSubquestionText(e.target.value, i, si)
                        }
                      />
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Body"
                      />
                    </div>
                  </div>
                ))}
            </div>
            {q.text && (
              <button
                className="text-xs py-0 my-0"
                onClick={() => this.handleAddSubquestion(i)}
              >
                Přidat podotázku
              </button>
            )}
          </div>
        ))}{' '}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddAssignment);

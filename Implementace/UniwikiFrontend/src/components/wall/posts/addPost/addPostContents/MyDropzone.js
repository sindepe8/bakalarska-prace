import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import { setPostContentFiles } from '../../../../../actions/addPost';

const MyDropzone = props => {
  const onDrop = useCallback(acceptedFiles => props.onDrop(acceptedFiles), []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {props.children}
    </div>
  );
};

export default MyDropzone;

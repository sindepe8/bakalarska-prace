import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { setPostContent } from '../../../../../actions/addPost';
import PostFiles from './PostFiles';
import AddPostFileDropzone from './AddPostFileDropzone';

class AddPostFreePostContent extends React.Component {
  render() {
    return (
      <div className="mt-3 d-flex align-items-top scroll-menu">
        <PostFiles files={this.props.addPost.content.files} />
        <AddPostFileDropzone />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPostFreePostContent);

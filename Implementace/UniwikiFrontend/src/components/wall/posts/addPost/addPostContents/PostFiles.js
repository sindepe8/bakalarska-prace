import React from 'react';
import UploadedFile from './UploadedFile';

const PostFiles = props =>
  props.files
    ? props.files.map(f => <UploadedFile file={f} key={f.src} />)
    : null;

export default PostFiles;

import React from 'react';
import {
  ExperiencePostType,
  ExamPostType,
  TestPostType,
  StudyMaterialPostType,
  FreePostType,
  SemesterWorkPostType
} from '../../../../../constants/postTypes';
import AddPostExperienceContent from './AddPostExperienceContent';
import AddPostExamContent from './AddPostExamContent';
import AddPostStudyMaterialContent from './AddPostStudyMaterialContent';
import AddPostSemesterWorkContent from './AddPostSemesterWorkContent';
import AddPostTestContent from './AddPostTestContent';
import AddPostFreePostContent from './AddPostFreePostContent';

class AddPostContent extends React.Component {
  constructor(props) {
    super(props);
  }

  getContent = () => {
    return this.content.getContent(); //todo
  };

  getContentComponent = () => {
    switch (this.props.postType) {
      case ExperiencePostType:
        return <AddPostExperienceContent />;
      case ExamPostType:
        return <AddPostExamContent />;
      case TestPostType:
        return <AddPostTestContent />;
      case StudyMaterialPostType:
        return <AddPostStudyMaterialContent />;
      case SemesterWorkPostType:
        return <AddPostSemesterWorkContent />;
      case FreePostType:
        return <AddPostFreePostContent />;
      default:
        throw new Error(
          'Enum not exhausted for value: "' + this.props.postType + '"'
        );
    }
  };

  render() {
    return this.getContentComponent();
  }
}

export default AddPostContent;

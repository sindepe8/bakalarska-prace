import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getDotIcon } from '../../../../../helpers/difficultyHelper';
import { connect } from 'react-redux';
import { setPostContent } from '../../../../../actions/addPost';

class AddPostExperienceContent extends React.Component {
  handleSemesterDifficultySelected = difficulty => {
    this.props.dispatch(setPostContent({ semesterDifficulty: difficulty }));
  };

  handleExamDifficultySelected = difficulty => {
    this.props.dispatch(setPostContent({ examDifficulty: difficulty }));
  };

  handleSemesterDifficultyRemove = () => {
    this.props.dispatch(setPostContent({ semesterDifficulty: undefined }));
  };

  handleExamDifficultyRemove = () => {
    this.props.dispatch(setPostContent({ examDifficulty: undefined }));
  };

  render() {
    return (
      <div className="mt-3">
        <div className="d-flex align-items-middle">
          <div>Obtížnost semestru:</div>
          <button onClick={() => this.handleSemesterDifficultySelected(1)}>
            <FontAwesomeIcon
              icon={getDotIcon(
                1,
                this.props.addPost.content.semesterDifficulty
              )}
            />
          </button>
          <button onClick={() => this.handleSemesterDifficultySelected(2)}>
            <FontAwesomeIcon
              icon={getDotIcon(
                2,
                this.props.addPost.content.semesterDifficulty
              )}
            />
          </button>
          <button onClick={() => this.handleSemesterDifficultySelected(3)}>
            <FontAwesomeIcon
              icon={getDotIcon(
                3,
                this.props.addPost.content.semesterDifficulty
              )}
            />
          </button>
          <button onClick={() => this.handleSemesterDifficultySelected(4)}>
            <FontAwesomeIcon
              icon={getDotIcon(
                4,
                this.props.addPost.content.semesterDifficulty
              )}
            />
          </button>
          <button onClick={() => this.handleSemesterDifficultySelected(5)}>
            <FontAwesomeIcon
              icon={getDotIcon(
                5,
                this.props.addPost.content.semesterDifficulty
              )}
            />
          </button>
          {this.props.addPost.content.semesterDifficulty && (
            <button
              className="ml-3"
              onClick={() => this.handleSemesterDifficultyRemove()}
            >
              <FontAwesomeIcon icon={['fas', 'times']} />
            </button>
          )}
        </div>
        <div className="d-flex align-items-middle">
          <div>Obtížnost zkoušky:</div>
          <button onClick={() => this.handleExamDifficultySelected(1)}>
            <FontAwesomeIcon
              icon={getDotIcon(1, this.props.addPost.content.examDifficulty)}
            />
          </button>
          <button onClick={() => this.handleExamDifficultySelected(2)}>
            <FontAwesomeIcon
              icon={getDotIcon(2, this.props.addPost.content.examDifficulty)}
            />
          </button>
          <button onClick={() => this.handleExamDifficultySelected(3)}>
            <FontAwesomeIcon
              icon={getDotIcon(3, this.props.addPost.content.examDifficulty)}
            />
          </button>
          <button onClick={() => this.handleExamDifficultySelected(4)}>
            <FontAwesomeIcon
              icon={getDotIcon(4, this.props.addPost.content.examDifficulty)}
            />
          </button>
          <button onClick={() => this.handleExamDifficultySelected(5)}>
            <FontAwesomeIcon
              icon={getDotIcon(5, this.props.addPost.content.examDifficulty)}
            />
          </button>
          {this.props.addPost.content.examDifficulty && (
            <button
              className="ml-3"
              onClick={() => this.handleExamDifficultyRemove()}
            >
              <FontAwesomeIcon icon={['fas', 'times']} />
            </button>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPostExperienceContent);

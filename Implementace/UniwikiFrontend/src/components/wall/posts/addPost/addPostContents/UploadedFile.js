import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const getPreview = file => {
  // if its a picture, create a preview
  if (file.type.startsWith('image')) {
    return <img src={file.src} />;
  }

  // Get the icon for the image
  let icon;
  const fileExtension = file.name
    .split('.')
    .pop()
    .toLowerCase();
  switch (fileExtension) {
    case 'docx':
    case 'docm':
    case 'dotx':
    case 'dotm':
    case 'docb':
      icon = ['fas', 'file-word'];
      break;
    case 'pptx':
    case 'pptm':
    case 'ppt':
    case 'potx':
      icon = ['fas', 'file-powerpoint'];
      break;
    case 'xls':
    case 'xlsb':
    case 'xlsm':
    case 'xlsx':
      icon = ['fas', 'file-excel'];
      break;
    case 'zip':
    case 'rar':
    case '7z':
    case 'sitx':
    case 'gz':
      icon = ['fas', 'file-archive'];
      break;
    case 'pdf':
      icon = ['fas', 'file-pdf'];
      break;
    case 'c':
    case 'cpp':
    case 'h':
    case 'hpp':
    case 'cs':
    case 'js':
    case 'jsx':
    case 'ts':
    case 'tsx':
    case 'php':
    case 'class':
    case 'java':
    case 'sh':
    case 'swift':
    case 'vb':
    case 'py':
    case 'r':
      icon = ['fas', 'file-code'];
      break;
    default:
      icon = ['fas', 'file'];
  }
  return <FontAwesomeIcon icon={icon} className="fa-3x" />;
};

class UploadedFile extends React.Component {
  render() {
    return (
      <a href="#" className="mr-3">
        <div className="add-post-item-to-upload">
          <div className="add-post-item-to-upload-content">
            {getPreview(this.props.file)}
          </div>
          <div className="add-post-item-to-upload-text mt-2">
            {this.props.file.name}
          </div>
        </div>
      </a>
    );
  }
}

export default UploadedFile;

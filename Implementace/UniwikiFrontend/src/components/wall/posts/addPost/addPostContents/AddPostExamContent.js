import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import AddPostFileDropzone from './AddPostFileDropzone';
import PostFiles from './PostFiles';
import AddAssignment from './AddAssignment';

class AddPostExamContent extends React.Component {
  render() {
    return (
      <div className="mt-3">
        <h4>Nahrát originál</h4>
        <div className="mt-3 d-flex align-items-top scroll-menu">
          <PostFiles files={this.props.addPost.content.files} />
          <AddPostFileDropzone />
        </div>
        <h4 className="mt-3">Zadání</h4>
        <AddAssignment />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPostExamContent);

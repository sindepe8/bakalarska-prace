import React from 'react';
import MyDropzone from './MyDropzone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { setPostContentFiles } from '../../../../../actions/addPost';

class AddPostFileDropzone extends React.Component {
  handleOnDrop = acceptedFiles => {
    let files = [...acceptedFiles];
    files = files.map(f => {
      f.src = URL.createObjectURL(f);
      return f;
    });

    this.props.dispatch(setPostContentFiles(files));

    const reader = new FileReader();

    reader.onabort = () => console.log('file reading was aborted');
    reader.onerror = () => console.log('file reading has failed');
    reader.onload = () => {
      // Do whatever you want with the file contents
      //const binaryStr = reader.result;
      //console.log(binaryStr);
    };

    //acceptedFiles.forEach(file => reader.readAsBinaryString(file));
  };

  render() {
    return (
      <div className="">
        <div className="add-post-item-to-upload">
          <MyDropzone onDrop={this.handleOnDrop}>
            <div className="add-post-item-to-upload-content add pointer">
              <FontAwesomeIcon icon={['fas', 'plus']} className="fa-2x" />
            </div>
          </MyDropzone>
          <div className="add-post-item-to-upload-text mt-2 color-secondary">
            Přidat {this.props.addPost.content.files && ' další'}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPostFileDropzone);

import React from 'react';
import {
  PostTypeIcon,
  getNameForPostType
} from '../../../../helpers/postType/postTypeHelper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { setPostType } from '../../../../actions/addPost';

class AddPostTypeIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  select = () => {
    this.props.dispatch(setPostType(this.props.postType));
  };

  deselect = () => {
    this.props.dispatch(setPostType(undefined));
  };

  handleClick = () => {
    if (this.props.addPost.postType) this.deselect();
    else this.select();
  };

  render() {
    return (
      <button
        className="add-post-type py-2 px-3 text-xs"
        onClick={this.handleClick}
      >
        <PostTypeIcon postType={this.props.postType} className="mr-3" />
        {getNameForPostType(this.props.postType)}
        {this.props.addPost.postType === this.props.postType && (
          <FontAwesomeIcon icon={['fas', 'times']} className="ml-3" />
        )}
      </button>
    );
  }
}

const mapStateToProps = state => {
  return {
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPostTypeIcon);

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AddPostTypeIcon from './AddPostTypeIcon';
import {
  ExamPostType,
  TestPostType,
  ExperiencePostType,
  StudyMaterialPostType,
  SemesterWorkPostType,
  FreePostType,
  AllPostTypes
} from '../../../../constants/postTypes';
import AddPostContent from './addPostContents/AddPostContent';
import { connect } from 'react-redux';
import PostCourse from '../PostCourse';
import PostFaculty from '../PostFaculty';
import { addPost } from '../../../../actions/post';
import PostModel from '../../../../models/postModel';
import ExperiencePostContentModel from '../../../../models/experiencePostContentModel';
import {
  setPostContent,
  setPostText,
  setPostType,
  clearAddPost
} from '../../../../actions/addPost';

class AddPost extends React.Component {
  constructor(props) {
    super(props);
  }

  get isVisible() {
    return (
      this.props.user.loggedUser &&
      (this.props.post.searchResults.query.university &&
        this.props.post.searchResults.query.faculty &&
        this.props.post.searchResults.query.course &&
        this.props.post.searchResults.query.faculty.name ===
          this.props.user.loggedUser.faculty.name &&
        this.props.post.searchResults.query.university.name ===
          this.props.user.loggedUser.faculty.university.name)
    );
  }

  handlePostTypeSelected = postType => {
    this.props.dispatch(setPostType(postType));
  };

  handlePostTypeDeselected = () => {
    this.props.dispatch(setPostType(undefined));
  };

  handleAddPost = () => {
    this.props.dispatch(
      addPost(
        new PostModel(
          new Date().getTime(),
          this.props.addPost.postType,
          this.props.post.searchResults.query.course,
          this.props.user.loggedUser,
          new Date(),
          this.props.addPost.text,
          0,
          [],
          this.props.addPost.content
        )
      )
    );
    this.props.dispatch(clearAddPost());
  };

  render() {
    return this.isVisible ? (
      <div className="post mb-3">
        <div className="post-header d-flex justify-content-between px-4 py-2 text-xs">
          <div>Přidat příspěvek</div>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="customCheck1"
            />
            <label className="custom-control-label" htmlFor="customCheck1">
              Anonymně
            </label>
          </div>
        </div>
        <div className="post-body pb-0">
          {
            /*this.state.selectedPostType &&*/ <div className="d-flex align-items-center">
              <img
                src={
                  this.props.user.loggedUser
                    ? this.props.user.loggedUser.profileImg
                    : '/images/main/profile.png'
                }
                alt="Unsinged icon"
                className="add-post-profile-img "
              />
              <div className="flex-grow-1 h-100 ml-3 border-top border-bottom">
                <input
                  type="text"
                  name="postText"
                  id="postText"
                  placeholder="Text k příspěvku"
                  className="add-post-input-text w-100 border-0 text-md px-3"
                  value={this.props.addPost.text}
                  onChange={e =>
                    this.props.dispatch(setPostText(e.target.value))
                  }
                />
              </div>
            </div>
          }
          {this.props.addPost.postType && (
            <AddPostContent
              postType={this.props.addPost.postType}
              onContentChange={c => (this.postContent = c)}
            />
          )}
          <div className="scroll-menu d-flex small mt-3 clearfix pb-3 align-items-center">
            {AllPostTypes.filter(
              type =>
                this.props.addPost.postType === undefined ||
                this.props.addPost.postType === type
            ).map(type => (
              <AddPostTypeIcon
                postType={type}
                postTypeSelected={this.handlePostTypeSelected}
                postTypeDeselected={this.handlePostTypeDeselected}
                key={type}
              />
            ))}

            {this.props.addPost.postType && (
              <React.Fragment>
                <PostFaculty
                  faculty={this.props.post.searchResults.query.faculty}
                />
                <PostCourse
                  course={this.props.post.searchResults.query.course}
                />
                <button
                  className="btn btn-primary btn-small ml-3"
                  onClick={this.handleAddPost}
                >
                  Přidat
                </button>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    ) : null;
  }
}

const mapStateToProps = state => {
  return {
    post: state.post,
    user: state.user,
    addPost: state.addPost
  };
};

export default connect(mapStateToProps)(AddPost);

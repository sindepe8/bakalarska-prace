import React from 'react';
import {
  ExperiencePostType,
  FreePostType,
  SemesterWorkPostType,
  StudyMaterialPostType,
  TestPostType,
  ExamPostType
} from '../../../constants/postTypes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PostTypePill = props => {
  let icon;
  let text;
  switch (props.postType) {
    case ExperiencePostType:
      icon = ['fas', 'grin-wink'];
      text = 'Zkušenost';
      break;
    case FreePostType:
      icon = ['fas', 'sticky-note'];
      text = 'Volný příspěvek';
      break;
    case SemesterWorkPostType:
      icon = ['fas', 'project-diagram'];
      text = 'Práce v semestru';
      break;
    case StudyMaterialPostType:
      icon = ['fas', 'file-alt'];
      text = 'Studijní materiál';
      break;
    case TestPostType:
      icon = ['fas', 'tasks'];
      text = 'Test';
      break;
    case ExamPostType:
      icon = ['fas', 'pencil-ruler'];
      text = 'Zkouška';
      break;
  }

  return (
    <div className="post-type">
      <FontAwesomeIcon icon={icon} className="mr-3" />
      {text}
    </div>
  );
};

export default PostTypePill;

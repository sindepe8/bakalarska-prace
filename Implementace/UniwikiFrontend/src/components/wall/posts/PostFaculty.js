import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PostFaculty = props => (
  <div className="text-sm ml-auto mr-3">
    <FontAwesomeIcon icon={['fas', 'university']} className="mr-2" />
    {props.faculty.university.name} {props.faculty.name}
  </div>
);

export default PostFaculty;

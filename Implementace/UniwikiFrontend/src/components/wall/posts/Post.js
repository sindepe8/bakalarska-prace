import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import {
  ExamPostType,
  ExperiencePostType,
  FreePostType,
  SemesterWorkPostType,
  StudyMaterialPostType,
  TestPostType
} from '../../../constants/postTypes';
import ExperiencePostContent from './ExperiencePostContent';
import PostTypePill from './PostTypePill';
import PostFaculty from './PostFaculty';
import PostCourse from './PostCourse';
import PostComment from './PostComment';
import SemesterWorkPostContent from './SemesterWorkPostContent';
import StudyMaterialPostContent from './StudyMaterialPostContent';
import FreePostContent from './FreePostPostContent';
import TestPostContent from './TestPostContent';
import ExamPostContent from './ExamPostContent';

function getPostConent(postModel) {
  console.log('Post is of type: ' + postModel.postType);
  switch (postModel.postType) {
    case ExperiencePostType:
      return <ExperiencePostContent content={postModel.content} />;
    case StudyMaterialPostType:
      return <StudyMaterialPostContent content={postModel.content} />;
    case SemesterWorkPostType:
      return <SemesterWorkPostContent content={postModel.content} />;
    case FreePostType:
      return <FreePostContent content={postModel.content} />;
    case TestPostType:
      return <TestPostContent content={postModel.content} />;
    case ExamPostType:
      return <ExamPostContent content={postModel.content} />;
  }
}

class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allCommentsVisible: false
    };
  }

  toggleVisibility = () => {
    this.setState(state => ({
      allCommentsVisible: !state.allCommentsVisible
    }));
  };

  render() {
    let postModel = this.props.postModel;
    let postType = postModel.postType;
    let postFaculty = postModel.course.faculty;
    let postCourse = postModel.course;
    let postContent = getPostConent(postModel);
    let postComments = postModel.comments;
    let postProfile = postModel.profile;

    return (
      <div className="post mb-3">
        {/* Post info */}
        <div className="post-body">
          <div className="d-flex align-items-center">
            <img
              src={postProfile.profileImg}
              alt="Profile picture"
              className="post-profile-image"
            />
            <div className="post-user-info px-3">
              <div className="post-user-name">
                {postProfile.firstName} {postProfile.familyName}
              </div>
              <div className="post-time">
                před 1h {/* TODO: Doplnit datum */}
              </div>
            </div>
            <PostTypePill postType={postType} />
          </div>
          <div className="post-text mt-2">{postModel.text}</div>
        </div>
        {/* Content */}
        {postContent}
        <hr className="my-0 py-0" />
        {/* Likes */}
        <div className="post-likes d-flex align-items-center mt-3">
          <div className="text-sm color-primary mr-3">
            <FontAwesomeIcon icon={['fas', 'thumbs-up']} className="mr-2" />
            {postModel.likesCount}
          </div>
          <div className="text-sm vertical-align-middle color-secondary">
            To se mi líbí
          </div>
          <PostFaculty faculty={postFaculty} />
          <PostCourse course={postCourse} />
        </div>
        <hr className="mb-0 pb-0" />
        {/* Comments */}
        <div className="post-body">
          <div className="text-sm mb-2">
            {postComments
              .slice(0, this.state.allCommentsVisible ? postComments.length : 1)
              .map(comment => (
                <PostComment comment={comment} key={comment.id} />
              ))}
          </div>
          {postComments.length > 1 &&
            (this.state.allCommentsVisible ? (
              <button onClick={this.toggleVisibility} className="mb-3 mt-0">
                Skrýt komentáře
              </button>
            ) : (
              <button onClick={this.toggleVisibility} className="mb-3 mt-0">
                Zobrazit všechny ({postComments.length}) komentáře
              </button>
            ))}
          <div className="d-flex align-items-center">
            <img
              src={
                this.props.user.loggedUser
                  ? this.props.user.loggedUser.profileImg
                  : '/images/main/profile.png'
              }
              alt="Unsinged icon"
              className="post-comment-profile-img"
            />
            <textarea
              name=""
              id=""
              cols="30"
              rows="1"
              placeholder="Napište komentář"
              className="post-wrtie-comment ml-3 text-sm px-4 py-2"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(Post);

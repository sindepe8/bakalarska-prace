import React from 'react';
import PostFiles from './addPost/addPostContents/PostFiles';
import Assignment from './Assignment';

const ExamPostContent = props => (
  <div className="px-3 mt-3">
    {props.content.files && (
      <div>
        <h4>Originál</h4>
        <div className="d-flex align-items-top scroll-menu">
          <PostFiles files={props.content.files} />
        </div>
      </div>
    )}
    <h4>Zadání</h4>
    {/* Questions */}
    <Assignment questions={props.content.questions} />
    <div />
  </div>
);

export default ExamPostContent;

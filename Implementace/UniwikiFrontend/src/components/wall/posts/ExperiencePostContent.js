import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function getDotIcon(dotNum, difficulty) {
  if (dotNum <= difficulty) return ['far', 'dot-circle'];
  return ['far', 'circle'];
}

const ExperiencePostContent = props => (
  <div className="post-body d-flex align-items-center mt-0 pt-0 text-sm">
    <div className="d-flex align-items-center">
      <div>Obtížnost semestru:</div>
      <FontAwesomeIcon
        icon={getDotIcon(1, props.content.semesterDifficulty)}
        className="ml-1"
      />
      <FontAwesomeIcon icon={getDotIcon(2, props.content.semesterDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(3, props.content.semesterDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(4, props.content.semesterDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(5, props.content.semesterDifficulty)} />
    </div>
    <div className="d-flex align-items-center ml-3">
      <div>Obtížnost zkoušky:</div>
      <FontAwesomeIcon
        icon={getDotIcon(1, props.content.examDifficulty)}
        className="ml-1"
      />
      <FontAwesomeIcon icon={getDotIcon(2, props.content.examDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(3, props.content.examDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(4, props.content.examDifficulty)} />
      <FontAwesomeIcon icon={getDotIcon(5, props.content.examDifficulty)} />
    </div>
  </div>
);

export default ExperiencePostContent;

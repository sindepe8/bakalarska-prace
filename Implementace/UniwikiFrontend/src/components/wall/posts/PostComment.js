import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';

const PostComment = props => {
  let comment = props.comment;
  let profile = comment.profile;
  return (
    <div>
      <div className="d-flex align-items-center">
        <img
          src={profile.profileImg}
          alt="Profile picture"
          className="post-comment-profile-img mr-3"
        />
        <div className="post-comment-text px-4 py-2 w-100">
          <NavLink to="/search" className="color-secondary bold-med mr-2">
            {profile.fullName}
          </NavLink>
          {comment.text}
        </div>
      </div>
      <div className="d-flex ml-5 pl-4 justify-content-between mt-1">
        <div className="d-flex">
          <div className="text-xs color-secondary">To se mi líbí</div>
          <div className="text-xs ml-3">
            56 min {/* TODO: Parse the time here */}
          </div>
        </div>
        <div className="likes-count-sm comment-likes mr-3">
          <FontAwesomeIcon icon={['fas', 'thumbs-up']} className="mr-2" />
          {comment.likesCount}
        </div>
      </div>
    </div>
  );
};

export default PostComment;

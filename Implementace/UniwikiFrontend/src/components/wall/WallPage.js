import React from 'react';
import { connect } from 'react-redux';
import SearchBox from '../SearchBox';
import AddPost from './posts/addPost/AddPost';
import Post from './posts/Post';
import DataService from '../../services/dataService';
import { NavLink } from 'react-router-dom';
import { deserializeQuery } from '../../helpers/searchQueryHelper';
import { setSearchQuery } from '../../actions/post';

class WallPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.updateFiltersFromQuery(this.props);
  }

  componentWillUpdate(nextProps) {
    this.updateFiltersFromQuery(nextProps);
  }

  updateFiltersFromQuery(nextProps) {
    if (
      this.lastUrl === nextProps.location.pathname &&
      this.lastSearch === nextProps.location.search
    )
      return; // nothing changed
    this.lastUrl = nextProps.location.pathname;
    this.lastSearch = nextProps.location.search;

    let query = deserializeQuery(
      nextProps.match.params,
      nextProps.location.search
    );

    this.props.dispatch(setSearchQuery(query));
  }

  render() {
    return (
      <div className="wall-page h-100 mt-0 pt-0">
        {/* <NavLink to="/search/vse">vse</NavLink>
        <br />
        <NavLink to="/search/cvut">cvut</NavLink>
        <br />
        <NavLink to="/search/cvut/fit">cvut/fit</NavLink>
        <br />
        <NavLink to="/search/cvut/fit/bi-lin">cvut/fit/lin</NavLink>
        <br />
        <NavLink to="/search/cvut/fit/bi-lin?q=popopo">
          cvut/fit/lin?q=popopo
        </NavLink> */}
        <div className="container py-3">
          <SearchBox />
          <hr className="mt-2 mb-3" />
          <AddPost />
          {this.props.post.searchResults.posts.map(postModel => (
            <Post postModel={postModel} key={postModel.id} />
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(WallPage);

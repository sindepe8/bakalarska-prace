import React from 'react';
import { connect } from 'react-redux';
import { requestLogin } from '../../actions/user';

class RequestLoginButton extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClick = () => {
    this.props.dispatch(requestLogin());
  };

  render() {
    return (
      <button className={this.props.className} onClick={this.handleClick}>
        {this.props.children}
      </button>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(RequestLoginButton);

import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import { hideModal } from '../../actions/modal';
import { ModalTypeLogin } from '../../constants/modalTypes';
import LoginModal from './LoginModal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import noScroll from 'no-scroll';

Modal.setAppElement('#app');
Modal.defaultStyles.overlay.backgroundColor = '#0009';
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '1rem',
    border: '1.5px solid #555',
    boxShadow: '10px 10px 20px 6px rgba(0,0,0,0.35)'
  }
};

class AppModal extends React.Component {
  getModalContent = () => {
    switch (this.props.modal.visibleModal) {
      case ModalTypeLogin:
        {
          return <LoginModal />;
        }
        break;
    }
  };

  hideModal = () => {
    this.props.dispatch(hideModal());
  };

  render() {
    if (this.props.modal.visibleModal) noScroll.on();
    else noScroll.off();

    return (
      <Modal
        isOpen={!!this.props.modal.visibleModal}
        shouldCloseOnOverlayClick={true}
        onRequestClose={this.hideModal}
        style={customStyles}
      >
        <div className="text-right">
          <button onClick={this.hideModal}>
            <FontAwesomeIcon className="text-dark" icon={['fas', 'times']} />
          </button>
        </div>
        {this.getModalContent()}
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    modal: state.modal
  };
};

export default connect(mapStateToProps)(AppModal);

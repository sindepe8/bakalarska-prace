import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/user';
import DataService from '../../services/dataService';
import { hideModal } from '../../actions/modal';

const LoginModal = props => (
  <div className="app-modal text-center">
    <h2>Přihlásit se</h2>
    <button className="button-login-fb mt-4 rounded">
      <div className="d-flex align-items-center text-sm">
        <FontAwesomeIcon icon={['fab', 'facebook']} className="mr-3 text-md" />
        <div className="text-center ml-3">Přes Facebook</div>
      </div>
    </button>
    <hr />
    <div className="input-group input-group-lg mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text" id="mail">
          @
        </span>
      </div>
      <input
        type="text"
        className="form-control"
        placeholder="Mail"
        aria-label="Mail"
        aria-describedby="mail"
      />
    </div>
    <div className="input-group input-group-lg mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text" id="password">
          <FontAwesomeIcon icon={['fas', 'key']} />
        </span>
      </div>
      <input
        type="password"
        className="form-control"
        placeholder="Heslo"
        aria-label="Password"
        aria-describedby="password"
      />
    </div>
    <button
      className="btn bg-color-primary btn-lg btn-block mx-auto text-white"
      onClick={() => {
        props.dispatch(loginUser(DataService.profiles[0]));
        props.dispatch(hideModal());
      }}
    >
      Přihlásit
    </button>
    <hr />
    <div>
      <a className="text-sm" href="#">
        Registrovat
      </a>
    </div>
    <a className="text-xs" href="#">
      Zapomněl(a) jste heslo?
    </a>
  </div>
);

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps)(LoginModal);

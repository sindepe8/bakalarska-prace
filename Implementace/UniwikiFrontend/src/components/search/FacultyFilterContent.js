import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getLinkUrl } from '../../helpers/searchQueryHelper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const FacultyItem = props => (
  <li className="d-flex">
    <NavLink
      className={'filter-item text-sm' + (props.isActive ? ' active' : '')}
      to={getLinkUrl({
        ...props.query,
        faculty: props.faculty
      })}
    >
      <div className="px-3 py-2">
        {props.faculty.name + ' - ' + props.faculty.fullName}
      </div>
    </NavLink>
    <NavLink
      className={'text-sm px-3 py-2' + (props.isActive ? ' active' : '')}
      to={getLinkUrl({
        ...props.query,
        faculty: undefined
      })}
    >
      {props.isActive && <FontAwesomeIcon icon={['fas', 'times']} />}
    </NavLink>
  </li>
);

const FacultyFilterContent = props => (
  <ul className="list-unstyled">
    {props.post.searchResults.query.university &&
      props.post.searchResults.query.university.faculties.map(f => (
        <FacultyItem
          faculty={f}
          key={f.name}
          query={props.post.searchResults.query}
          isActive={
            props.post.searchResults.query.faculty &&
            props.post.searchResults.query.faculty.name === f.name
          }
        />
      ))}
  </ul>
);

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(FacultyFilterContent);

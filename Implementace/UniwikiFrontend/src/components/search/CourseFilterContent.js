import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getLinkUrl } from '../../helpers/searchQueryHelper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CourseItem = props => (
  <li className="d-flex">
    <NavLink
      className={'filter-item text-sm' + (props.isActive ? ' active' : '')}
      to={getLinkUrl({
        ...props.query,
        course: props.course
      })}
    >
      <div className="px-3 py-2">
        {props.course.code + ' - ' + props.course.name}
      </div>
    </NavLink>
    <NavLink
      className={'text-sm px-3 py-2' + (props.isActive ? ' active' : '')}
      to={getLinkUrl({
        ...props.query,
        course: undefined
      })}
    >
      {props.isActive && <FontAwesomeIcon icon={['fas', 'times']} />}
    </NavLink>
  </li>
);

class CourseFilterContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  handleTextChanged = e => {
    let newState = { text: e.target.value };
    this.setState(() => newState);
  };

  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="Název nebo kód předmětu"
          className="w-100 text-sm mx-3"
          value={this.props.text}
          onChange={this.handleTextChanged}
        />
        <ul className="list-unstyled">
          {this.props.post.searchResults.query.faculty &&
            this.props.post.searchResults.query.faculty.courses
              .filter(
                c =>
                  c.name
                    .toLowerCase()
                    .includes(this.state.text.toLowerCase()) ||
                  c.code.toLowerCase().includes(this.state.text.toLowerCase())
              )
              .map(c => (
                <CourseItem
                  course={c}
                  query={this.props.post.searchResults.query}
                  key={c.urlCode}
                  isActive={
                    this.props.post.searchResults.query.course &&
                    c.code === this.props.post.searchResults.query.course.code
                  }
                />
              ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(CourseFilterContent);

import React from 'react';
import DataService from '../../services/dataService';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { getLinkUrl } from '../../helpers/searchQueryHelper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const UniversityItem = props => (
  <li className="d-flex align-items-center">
    <NavLink
      className={
        'filter-item text-sm px-3 py-2' + (props.isActive ? ' active' : '')
      }
      to={getLinkUrl({
        ...props.query,
        university: props.university
      })}
    >
      <img
        src={props.university.img}
        alt={props.university.imgAlt}
        className="filter-uni-img mr-3 mb-2"
      />
      {props.university.name}
      {' - ' + props.university.fullName}
    </NavLink>
    <NavLink
      className={'text-sm px-3 py-2' + (props.isActive ? ' active' : '')}
      to={getLinkUrl({
        ...props.query,
        university: undefined
      })}
    >
      {props.isActive && <FontAwesomeIcon icon={['fas', 'times']} />}
    </NavLink>
  </li>
);

const UniversityFilterContent = props => (
  <ul className="list-unstyled">
    {DataService.getUniversities().map(u => (
      <UniversityItem
        university={u}
        isActive={
          props.post.searchResults.query.university &&
          props.post.searchResults.query.university.name === u.name
        }
        key={u.name}
        query={props.post.searchResults.query}
      />
    ))}
  </ul>
);

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(UniversityFilterContent);

import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SearchBox from '../SearchBox';
import { UniversityTeaser } from './UniversityTeaser';
import DataService from '../../services/dataService';

const MainPage = () => (
  <div className="main-page">
    <div className="bg-main-search">
      <div className="container min-vh-60 p-0 d-flex align-items-center">
        <div className="my-auto mx-auto">
          <h1 className="text-center mb-5">Hledejte studijní materiály</h1>
          <SearchBox />
        </div>
      </div>
    </div>
    <div>
      <div className="container pb-5">
        <h2 className="my-5 text-center">Naše univerzity</h2>
        <div className="row">
          {DataService.getUniversities()
            .slice(0, 4)
            .map((u, i) => (
              <div
                className={'col-2' + (i === 0 ? ' offset-2' : '')}
                key={u.urlName}
              >
                <UniversityTeaser university={u} />
              </div>
            ))}
        </div>
      </div>
    </div>
  </div>
);

export default MainPage;

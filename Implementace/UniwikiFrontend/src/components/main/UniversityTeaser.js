import React from 'react';
import { NavLink } from 'react-router-dom';

export const UniversityTeaser = props => (
  <div className="d-flex flex-column align-items-center">
    <NavLink to={'/search/' + props.university.urlName}>
      <img
        src={props.university.img}
        alt={props.university.imgAlt}
        className="main-uni-img"
      />
      <h3 className="text-center mt-3 text-sm">{props.university.name}</h3>
    </NavLink>
  </div>
);

import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
  <div className="bg-dark text-secondary text-center py-3">&copy; Uniwiki</div>
);

export default Footer;

import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import RequestLoginButton from './hoc/RequestLoginButton';
import { connect } from 'react-redux';
import { loginUser } from '../actions/user';
import DataService from '../services/dataService';
import HeaderLogin from './HeaderLogin';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <header>
        <nav className="navbar navbar-expand-sm navbar-light">
          <div className="container">
            <NavLink className="mr-auto text-lg text-white" to="/" exact={true}>
              Uniwiki
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarToggle"
              aria-controls="navbarToggle"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon color-white" />
            </button>
            <HeaderLogin />
          </div>
        </nav>
        <hr className="p-0 m-0" />
      </header>
    );
  }
}

export default Header;

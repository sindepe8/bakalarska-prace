import React from 'react';
const BasicLayout = props => {
  return (
    <div className="p-0 m-0 min-vh-100 basic-layout">{props.children}</div>
  );
};

export default BasicLayout;

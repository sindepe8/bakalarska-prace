import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { setSearchAutocompleteText } from '../actions/post';
import SearchAutocomplete from './SearchAutocomplete';
import { NavLink } from 'react-router-dom';
import SearchFilters from './SearchFilters';
import { getLinkUrl } from '../helpers/searchQueryHelper';

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div autoComplete="off" className="border-0">
          <div className="autocomplete input-group input-group-lg mt-3">
            <div className="input-group-prepend search-box-icon">
              <FontAwesomeIcon
                icon={['fas', 'search']}
                className="mx-3 my-auto"
              />
            </div>
            <input
              type="text"
              className="form-control border-0 search-box-input"
              placeholder="Název předmětu, tématu, univerzity"
              autoFocus={true}
              autoComplete="off"
              id="myInput"
              name="myCountry"
              value={this.props.post.autocomplete.text}
              onChange={e => {
                this.props.dispatch(setSearchAutocompleteText(e.target.value));
              }}
            />
            <div>
              <NavLink
                to={getLinkUrl({
                  ...this.props.post.searchResults.query,
                  queryText: this.props.post.autocomplete.text
                })}
                className="btn search-box-button color-secondary h-100"
              >
                <FontAwesomeIcon
                  icon={['fas', 'arrow-right']}
                  className="mx-3 my-auto h-100"
                />
              </NavLink>
            </div>
            <SearchAutocomplete />
          </div>
        </div>
        <div className="mb-0">
          <SearchFilters />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(SearchBox);

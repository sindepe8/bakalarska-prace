import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import RequestLoginButton from './hoc/RequestLoginButton';
import { loginUser } from '../actions/user';
import DataService from '../services/dataService';

class HeaderLogin extends React.Component {
  state = {};
  render() {
    let user = this.props.user.loggedUser;

    return (
      <div className="collapse navbar-collapse" id="navbarToggle">
        {!user ? (
          <ul className="navbar-nav ml-auto mt-lg-0">
            <li className="nav-item">
              <button
                className="mr-3 color-white text-md"
                onClick={() =>
                  this.props.dispatch(loginUser(DataService.profiles[0]))
                }
              >
                <FontAwesomeIcon icon={['fab', 'facebook']} className="fa-lg" />
              </button>
            </li>
            <li className="nav-item">
              <RequestLoginButton className="btn btn-outline-light text-sm">
                Přihlásit se
              </RequestLoginButton>
            </li>
          </ul>
        ) : (
          <div className="navbar-nav ml-auto mt-lg-0 text-sm d-flex align-items-center">
            <img className="header-profile-img mr-3" src={user.profileImg} />
            <div>{user.fullName}</div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(HeaderLogin);

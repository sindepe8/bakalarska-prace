import React from 'react';
import { connect } from 'react-redux';

const SearchAutocomplete = props => (
  <div className="autocomplete-items">
    <div className="list-group">
      {props.post.autocomplete.results.map(term => (
        <a
          href="#"
          className="list-group-item list-group-item-action"
          key={term}
        >
          {term}
        </a>
      ))}
    </div>
  </div>
);

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(SearchAutocomplete);

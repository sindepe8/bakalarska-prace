import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import UniversityFilterContent from './search/UniversityFilterContent';
import CourseFilterContent from './search/CourseFilterContent';
import SortByFilterContent from './search/SortByFilterContent';
import FacultyFilterContent from './search/FacultyFilterContent';
import TypeFilterContent from './search/TypeFilterContent';

const getUniversityName = query => {
  return query.university === undefined ? undefined : query.university.name;
};

const getFacultyName = query => {
  return query.faculty === undefined ? undefined : query.faculty.name;
};

const getCourseName = query => {
  return query.course === undefined ? undefined : query.course.name;
};

const UniversityFilterName = 'University';
const FacultyFilterName = 'Faculty';
const CourseFilterName = 'Course';
const TypeFilterName = 'Type';
const SortByName = 'Sort by';

class SearchFilters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeFilter: null
    };
  }

  handleFilterMenuSelection = filterName => {
    let newFilterName = filterName;

    // Deselect if trying to select the same filter
    if (this.state.activeFilter === filterName) newFilterName = null;

    this.setState(() => ({
      activeFilter: newFilterName
    }));
  };

  getActiveMenu = () => {
    let activeFilter = this.state.activeFilter;
    if (!activeFilter) return null;

    switch (this.state.activeFilter) {
      case UniversityFilterName:
        return <UniversityFilterContent />;
      case FacultyFilterName:
        return <FacultyFilterContent />;
      case CourseFilterName:
        return <CourseFilterContent />;
      case TypeFilterName:
        return <TypeFilterContent />;
      case SortByName:
        return <SortByFilterContent />;
      default:
        throw new Error('Enum not exhausted: ' + this.state.activeFilter);
    }
  };

  render() {
    let query = this.props.post.searchResults.query;
    let universityName = getUniversityName(query);
    let facultyName = getFacultyName(query);
    let courseName = getCourseName(query);

    let activeFilter = this.state.activeFilter;
    let activeFilterMenu = this.getActiveMenu();
    return (
      <div className="m-0 p-0">
        <div autoComplete="off" className="border-0 text-sm m-0 p-0">
          {/* Univerzity */}
          <button
            className={
              'filter-button' +
              (activeFilter === UniversityFilterName ? ' active' : '')
            }
            onClick={() => this.handleFilterMenuSelection(UniversityFilterName)}
          >
            {universityName || 'Univerzita'}
          </button>

          {/* Faculty */}
          {universityName !== undefined ? (
            <button
              className={
                'filter-button' +
                (activeFilter === FacultyFilterName ? ' active' : '')
              }
              type="button"
              onClick={() => this.handleFilterMenuSelection(FacultyFilterName)}
            >
              {facultyName !== undefined ? facultyName : 'Fakulta'}
            </button>
          ) : (
            undefined
          )}

          {/* Course */}
          {facultyName !== undefined ? (
            <button
              className={
                'filter-button' +
                (activeFilter === CourseFilterName ? ' active' : '')
              }
              onClick={() => this.handleFilterMenuSelection(CourseFilterName)}
            >
              {courseName !== undefined ? courseName : 'Předmět'}
            </button>
          ) : (
            undefined
          )}

          <div className="divider" />

          {/* Type */}
          <button
            className={
              'filter-button' +
              (activeFilter === TypeFilterName ? ' active' : '')
            }
            onClick={() => this.handleFilterMenuSelection(TypeFilterName)}
          >
            Zobrazit vše
          </button>

          {/* Sort by */}
          <button
            className={
              'filter-button' + (activeFilter === SortByName ? ' active' : '')
            }
            onClick={() => this.handleFilterMenuSelection(SortByName)}
          >
            Od nejnovějšího
          </button>
        </div>
        {activeFilterMenu && (
          <div className="filter-items-body py-2">{activeFilterMenu}</div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

export default connect(mapStateToProps)(SearchFilters);

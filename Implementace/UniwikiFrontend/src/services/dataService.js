import PostModel from '../models/postModel';
import '../constants/postTypes';
import ExperiencePostContentModel from '../models/experiencePostContentModel';
import { ExperiencePostType, FreePostType } from '../constants/postTypes';
import ProfileModel from '../models/profileModel';
import UniversityModel from '../models/universityModel';
import FacultyModel from '../models/facultyModel';
import CourseModel from '../models/courseModel';
import PostCommentModel from '../models/postCommentModel';

class DataService {
  constructor() {
    // CVUT FACULTIES
    let cvutFit = new FacultyModel(
      this.generateId(),
      'FIT',
      'Fakulta informačních technologií'
    );
    let cvutFa = new FacultyModel(
      this.generateId(),
      'FA',
      'Fakulta architektury'
    );
    let cvutFbmi = new FacultyModel(
      this.generateId(),
      'FBMI',
      'Fakulta biomedicínského inženýrství'
    );
    let cvutFd = new FacultyModel(this.generateId(), 'FD', 'Fakulta dopravní');
    let cvutFel = new FacultyModel(
      this.generateId(),
      'FEL',
      'Fakulta elektrotechnická'
    );
    let cvutFjfi = new FacultyModel(
      this.generateId(),
      'FJFI',
      'Fakulta jaderná a fyzikálně inženýrská'
    );
    let cvutFs = new FacultyModel(this.generateId(), 'FS', 'Fakulta strojní');
    let cvutFsv = new FacultyModel(
      this.generateId(),
      'FSv',
      'Fakulta stavební'
    );

    let cvutFaculties = [
      cvutFa,
      cvutFbmi,
      cvutFd,
      cvutFel,
      cvutFit,
      cvutFjfi,
      cvutFs,
      cvutFsv
    ];
    // \CVUT FACULTIES

    // VSE FACULTIES
    let vseFis = new FacultyModel(
      this.generateId(),
      'FIS',
      'Fakulta Informatiky a Statistiky'
    );

    let vseFaculties = [vseFis];
    // \VSE FACULTIES

    // UNIVERSITIES
    let cvut = new UniversityModel(
      this.generateId(),
      'ČVUT',
      'České vysoké učení technické',
      cvutFaculties,
      '/images/main/universities/logo-cvut.jpg',
      'ČVUT logo'
    );

    let vse = new UniversityModel(
      this.generateId(),
      'VŠE',
      'Vysoká škola ekonomická',
      vseFaculties,
      '/images/main/universities/logo-vse.png',
      'VŠE logo'
    );

    this._universities = [
      vse,
      new UniversityModel(
        this.generateId(),
        'ČZU',
        'Česká zemědělská univerzita',
        [],
        '/images/main/universities/logo-czu.jpg',
        'ČZU logo'
      ),
      cvut,
      new UniversityModel(
        this.generateId(),
        'UK',
        'Univerzita Karlova',
        [],
        '/images/main/universities/logo-uk.png',
        'UK logo'
      )
    ];
    // \UNIVERSITIES

    // CVUT FIT COURSES
    let lin = new CourseModel(
      this.generateId(),
      'BI-LIN',
      'Lineární algebra',
      cvutFit
    );
    let t3d = new CourseModel(this.generateId(), 'BI-3DT', '3D Tisk', cvutFit);
    let mlo = new CourseModel(
      this.generateId(),
      'BI-MLO',
      'Matematická logika',
      cvutFit
    );
    let pa1 = new CourseModel(
      this.generateId(),
      'BI-PA1',
      'Programování a algoritmizace 1',
      cvutFit
    );
    let pa2 = new CourseModel(
      this.generateId(),
      'BI-PA2',
      'Programování a algoritmizace 2',
      cvutFit
    );
    let osy = new CourseModel(
      this.generateId(),
      'BI-OSY',
      'Operační systémy',
      cvutFit
    );
    let cvutFitCourses = [lin, t3d, mlo, pa1, pa2, osy];
    cvutFit.setCourses(cvutFitCourses);
    // \CVUT FIT COURSES

    // VSE FIS COURSES
    let ner = new CourseModel(
      this.generateId(),
      '11F450',
      'Nástroje pro ekonomická rozhodování',
      vseFis
    );

    let vseFisCourses = [ner];
    vseFis.setCourses(vseFisCourses);
    // \VSE FIS COURSES

    // PROFILES
    let profileAlena = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user1.jpg',
      'Alena',
      'Zelená',
      'alenazelena1',
      'alena.zelena@fit.cvut.cz',
      ['martnovak@fit.cvut.cz'],
      cvutFit
    );

    let profileBarbora = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user2.jpg',
      'Barbora',
      'Nováková',
      'martanovakova1',
      'marta.novakova@gmail.com',
      ['martnovak@fit.cvut.cz'],
      cvutFit
    );

    let profileCecilie = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user3.jpg',
      'Cecílie',
      'Veselá',
      'cecilievesela1',
      'cecilievesela@fit.cvut.cz',
      ['cecilievesela@fit.cvut.cz'],
      cvutFit
    );

    let profileDaniel = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user4.jpg',
      'Daniel',
      'Jakubec',
      'danieljakub3',
      'dan.jak.ub@seznam.cz',
      ['danieljakub3@fit.cvut.cz'],
      cvutFit
    );

    let profileEma = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user5.jpg',
      'Ema',
      'Srbková',
      'emasrbk0',
      'emasrbkova@gmail.com',
      ['emasrb@fit.cvut.cz'],
      cvutFit
    );

    let profileFranta = new ProfileModel(
      this.generateId(),
      '/images/wall/fakeProfile/fake-user6.jpg',
      'Franta',
      'Lutz',
      'franlutz',
      'franta.lutz@atlas.cz',
      ['frantlutz@fit.cvut.cz'],
      cvutFit
    );

    this.profiles = [
      profileAlena,
      profileBarbora,
      profileCecilie,
      profileDaniel,
      profileEma,
      profileFranta
    ];
    // \PROFILES

    this._posts = [
      new PostModel(
        10001,
        ExperiencePostType,
        lin,
        profileCecilie,
        new Date(),
        'Zkouška byla celkem jednoduchá :)).',
        11,
        [
          new PostCommentModel(
            this.generateId(),
            profileAlena,
            'Velmi těžké zadání na předtermín',
            4,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileBarbora,
            'Ale ne, zas tak těžké to nebylo.',
            1,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileAlena,
            'Vždyť tu trojku musíš počítat asi hodinu',
            2,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileBarbora,
            'To je pravda. Ale všechny ostatní úlohy máš hned.',
            0,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileFranta,
            'Tam se pouze použije pár triků a je to jednodušší než se zdá. Zrovna v tomto bych derivoval, integroval a násobil.',
            8,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileBarbora,
            'Aha lol.',
            0,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10001, 3, 4)
      ),
      new PostModel(
        10002,
        FreePostType,
        lin,
        profileFranta,
        new Date(),
        'Nemáte někdo náhodou zápisky z minulé přednášky?',
        1,
        [
          new PostCommentModel(
            this.generateId(),
            profileAlena,
            'Nějaké bych našla, nasdílím je.',
            4,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10002, 3, 4)
      ),
      new PostModel(
        10003,
        ExperiencePostType,
        lin,
        profileDaniel,
        new Date(),
        'Tenhle předmět dal fakt zabrat. Doporučuji to nepodcenit, abyste měli o zkouškovém alespoň chvilku volna.',
        7,
        [
          new PostCommentModel(
            this.generateId(),
            profileAlena,
            'Amen.',
            2,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10003, 3, 4)
      ),
      new PostModel(
        10004,
        ExperiencePostType,
        lin,
        profileEma,
        new Date(),
        'Pokud se na to jako 99 % lidí nevykašlete, tak to je úplně v pohodě předmět.',
        11,
        [],
        new ExperiencePostContentModel(10004, 3, 4)
      ),
      new PostModel(
        10005,
        ExperiencePostType,
        lin,
        profileAlena,
        new Date(),
        'Doporučuji projít všechny testy z Uniwiki, bez nich bych neměl šanci.',
        5,
        [
          new PostCommentModel(
            this.generateId(),
            profileBarbora,
            'A k tomu se učit ze studijních materiálů!',
            3,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10005, 3, 4)
      ),
      new PostModel(
        10006,
        ExperiencePostType,
        t3d,
        profileCecilie,
        new Date(),
        'Určitě oceňuji neskutečnou ochotu vyučujících.',
        11,
        [
          new PostCommentModel(
            this.generateId(),
            profileDaniel,
            'Takový přístup jsem na jiné škole nezažil.',
            4,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileCecilie,
            'Přesně tak',
            1,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10006, 3, 4)
      ),
      new PostModel(
        10007,
        ExperiencePostType,
        t3d,
        profileBarbora,
        new Date(),
        'Naprosto úžasný předmět.',
        8,
        [
          new PostCommentModel(
            this.generateId(),
            profileDaniel,
            'Jen pochybuji o praktickém využití.',
            2,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10007, 3, 4)
      ),
      new PostModel(
        10008,
        ExperiencePostType,
        t3d,
        profileCecilie,
        new Date(),
        'Neskutečně záživný předmět.',
        11,
        [],
        new ExperiencePostContentModel(10008, 3, 4)
      ),
      new PostModel(
        10009,
        ExperiencePostType,
        osy,
        profileCecilie,
        new Date(),
        'Vyučující jsou úplně super.',
        11,
        [
          new PostCommentModel(
            this.generateId(),
            profileDaniel,
            'Kdykoliv, když potřebujete, pomůžou.',
            4,
            new Date()
          ),
          new PostCommentModel(
            this.generateId(),
            profileCecilie,
            'To potvrzuji',
            1,
            new Date()
          )
        ],
        new ExperiencePostContentModel(10009, 3, 4)
      ),
      new PostModel(
        10010,
        ExperiencePostType,
        ner,
        profileCecilie,
        new Date(),
        'OSY jsou naprosto skvělé.',
        11,
        [],
        new ExperiencePostContentModel(10010, 3, 4)
      )
    ].reverse();
  }

  lastId = 1;
  generateId = () => {
    return this.lastId++;
  };

  getProfiles = () => {
    return this.getProfiles.slice(0);
  };

  getUniversities = () => {
    return this._universities.slice(0);
  };

  getPosts = q => {
    return this._posts.filter(
      p =>
        (!q.university ||
          p.course.faculty.university.name === q.university.name) &&
        (!q.faculty || p.course.faculty.name === q.faculty.name) &&
        (!q.course || p.course.name === q.course.name)
    );
  };

  addPost = post => {
    this._posts.unshift(post);
  };

  getAllPosts = () => {
    return this._posts.slice(0);
  };
}
let instance = new DataService();

export default instance;

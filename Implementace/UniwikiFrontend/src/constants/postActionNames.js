export const setSearchQueryAction = 'SET_SEARCH_QUERY';
export const setSearchAutocompleteTextAction = 'SET_SEARCH_AUTOCOMPLETE_TEXT';
export const addPostAction = 'ADD_POST';

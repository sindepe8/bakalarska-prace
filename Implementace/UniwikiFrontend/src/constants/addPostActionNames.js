export const setPostTextAction = 'SET_POST_TEXT';
export const setPostContentAction = 'SET_POST_CONTENT';
export const setPostTypeAction = 'SET_POST_TYPE';
export const clearAddPostAction = 'CLEAR_ADD_POST';
export const setPostContentFilesAction = 'SET_POST_CONTENT_FILES';
export const setAssignmentQuestionAction = 'SET_ASSIGNMENT_QUESTION';
export const setAssignmentSubquestionAction = 'SET_ASSIGNMENT_SUBQUESTION';
export const createAssignmentSubquestionAction =
  'CREATE_ASSIGNMENT_SUBQUESTION';

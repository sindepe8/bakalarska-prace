export const ExperiencePostType = 'ExperiencePost';
export const ExamPostType = 'ExamPost';
export const TestPostType = 'TestPost';
export const StudyMaterialPostType = 'StudyMaterialPost';
export const SemesterWorkPostType = 'SemesterWorkPost';
export const FreePostType = 'FreePost';

export const AllPostTypes = [
  ExperiencePostType,
  ExamPostType,
  TestPostType,
  StudyMaterialPostType,
  SemesterWorkPostType,
  FreePostType
];

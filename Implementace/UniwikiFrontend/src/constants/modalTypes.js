export const ModalTypeLogin = 'ModalTypeLogin';
export const ModalTypeRegistration = 'ModalTypeRegistration';
export const ModalTypeInputSchoolMail = 'ModalTypeInputSchoolMail';
export const ModalTypePleaseVerifySchoolMail = 'PleaseVerifySchoolMail';
export const ModalTypePleaseVerifyMail = 'PleaseVerifyMail';
